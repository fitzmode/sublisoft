import React, { createContext, useReducer, useEffect } from 'react';
import { ShapeTypes, PencilTypes } from '../models/models';
import WebfontLoader from '@dr-kobros/react-webfont-loader';
import * as shortid from 'shortid'
import useUpdateEffect from '../utils';

const zoomValues = Array.from(new Array(10), (x,i) => (i+1)*10);

const families =  ['Source Sans Pro:300,600', 'Karla', 'Modak'];
const fonts = families.map(font => font.substring(0, font.indexOf(':')) || font);


export interface ISettingsState {
    selectedZoom:number;
}

export enum SettingsActions {
    ChangeZoom = 'CHANGE_ZOOM'
}




type AppProps = {

}

export interface IAction { payload?: any; type: SettingsActions;};

const reducer = (state:ISettingsState, action: IAction ): ISettingsState => {
    switch (action.type) {
    case SettingsActions.ChangeZoom: 
        return {...state, selectedZoom: action.payload}
        default:
            throw new Error(`Unhandled action: ${action.type}`);
    }
}

const initialState: ISettingsState = {
    selectedZoom: 1,
   }

export const SettingsContext:any = createContext({
    state: initialState,
})

export const SettingsContextProvider : React.FC<AppProps> = (props) => {
 const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <SettingsContext.Provider value={[ state, dispatch ]}>
        {props.children}
    </SettingsContext.Provider>
  );
}