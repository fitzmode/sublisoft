import React, { useContext , useState} from 'react';
import { AppContext, IAppState, IAction, EditorActions } 
from '../context/context';
import MaterialIcon from 'material-icons-react';
import Popover from 'react-tiny-popover'
import { SketchPicker } from "react-color";
import { Button } from "react-bootstrap";
import { FilterComponent } from '../components/filter';
import { text_props, align_props, font_styles, text_decoration } from '../utils';

export const TextTools = () => {
    const [{ selectedObject, fonts,  }, dispatch] : [IAppState, React.Dispatch<IAction>] = useContext(AppContext) 
    const [{  textDecorations , isPopoverOpen}, setState] = useState({ 
        loaded: false, 
        isPopoverOpen: false,
        width: 0,
         height: 0, src: '',  textDecorations: [{name: 'U', key:'underline'},{name: 'S', key:'line-through'}],lines:[], drawing:false});

    const onFontChange = ($event: React.FormEvent<HTMLSelectElement>) => {
        dispatch({type: EditorActions.UpdateShape, payload:{ fontFamily: $event.currentTarget.value }})
    }


    const onOpacityChange = ($event: React.FormEvent<HTMLInputElement>) => {
        dispatch({type: EditorActions.UpdateShape, payload:{ opacity: parseFloat($event.currentTarget.value) }})
    }

  return (
    <>
    
    <select name="" id="" value={selectedObject.fontFamily} onChange={onFontChange}>
            {
                fonts.map(font => (
                    <option key={font} value={font}>{font}</option>
                ))
            }
        </select> 

         <FilterComponent 
            id={'opacity'}
            selected={selectedObject} 
            dispatch={dispatch} 
            min={0}
            icon="opacity"
            step={0.05}
            max={1}
            /> 
        
        { text_props.map(prop => (
         <FilterComponent
            key={prop.id}
            selected={selectedObject} 
            dispatch={dispatch} 
            {...prop}/> ))}

{
    align_props.map(align => (
    <Button 
    variant="link"
    onClick={() => dispatch({type: EditorActions.UpdateShape, payload: { align: align.id }})} >
    <MaterialIcon icon={align.icon} size={24} color='#fff'/>
    </Button>
    ))
}


{
    font_styles.map(style => (
    <Button 
    variant="link"
    onClick={() => {
        console.log("Font styles", style)
        dispatch({type: EditorActions.UpdateShape, payload: { fontStyle: 
        selectedObject.fontStyle.includes(style.id) ? selectedObject.fontStyle.replace(style.id ,''): `${selectedObject.fontStyle} ${style.id}` }})}} >
        <MaterialIcon icon={style.icon} size={24} color='#fff'/>
    </Button>
    ))
}

{
    text_decoration.map(decoration => (
    <Button 
    variant="link"
    onClick={() => {
 
        dispatch({type: EditorActions.UpdateShape, payload: { textDecoration: 
        selectedObject.textDecoration.includes(decoration.id) ? selectedObject.textDecoration.replace(decoration.id ,''): `${selectedObject.textDecoration} ${decoration.id}` }})}} >
        <MaterialIcon icon={decoration.icon} size={24} color='#fff'/>
    </Button>
    ))
}

<Popover
    isOpen={isPopoverOpen}
    position={'bottom'} // preferred position
    onClickOutside={() => {
        setState(state => ({ 
            ...state,
            isPopoverOpen: false }))
            
    }}
    content={({ position, targetRect, popoverRect }) => (
            <div style={{ background:'#373842', padding: 5}}>
            <SketchPicker  color={selectedObject.color} onChange={({ hex }) => {
        dispatch({ type: EditorActions.UpdateShape, payload: {color: hex }})
    }} /> 
            </div>
    )}
>
    <Button 
    variant="link"
 
    onClick={() => {
 setState(state => ({ 
    ...state,
    isPopoverOpen: !isPopoverOpen }))}
    }
   >
        <MaterialIcon  icon={'color_lens'} size={24} color={selectedObject.color}/>
    </Button>
</Popover>


    </>
  );
}
