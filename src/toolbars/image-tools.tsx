import React, { useContext, useRef, useEffect, useState } from 'react';
import { AppContext, IAppState, IAction, EditorActions } from '../context/context';
import { FilterComponent } from '../components/filter';
import { filters } from '../utils';

export const ImageTools = () => {
    const [{ selectedObject }, dispatch] : [IAppState, React.Dispatch<IAction>] = useContext(AppContext) 
    const div = useRef(null)
    const [{ width, height}, setState] = useState({width: 0, height: 0})

    useEffect(() => {
      if(div.current) {
        const dimensions = div.current.getBoundingClientRect()
        setState(state => ({
          ...state,
          width: dimensions.width,
          height: dimensions.height
        }))
      }
    }, [div])
  return (
    <>
  
{ filters.map(filter => (
         <FilterComponent 
            key={filter.id}
            selected={selectedObject} 
            dispatch={dispatch} 
            {...filter}/> ))}
    
    </>
  );
}
