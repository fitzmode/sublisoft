import React, { useContext , useState} from 'react';
import { AppContext, IAppState, IAction, EditorActions } 
from '../context/context';
import MaterialIcon from 'material-icons-react';
import Popover from 'react-tiny-popover'
import { SketchPicker } from "react-color";
import { Button } from "react-bootstrap";
import { FilterComponent } from '../components/filter';
import { text_props, align_props, font_styles } from '../utils';

export const CropTools = () => {
    const [{ selectedObject, fonts,  }, dispatch] : [IAppState, React.Dispatch<IAction>] = useContext(AppContext) 
    const [{  textDecorations , isPopoverOpen}, setState] = useState({ 
        loaded: false, 
        isPopoverOpen: false,
        width: 0,
         height: 0, src: '',  textDecorations: [{name: 'U', key:'underline'},{name: 'S', key:'line-through'}],lines:[], drawing:false});

    const onFontChange = ($event: React.FormEvent<HTMLSelectElement>) => {
        dispatch({type: EditorActions.UpdateShape, payload:{ fontFamily: $event.currentTarget.value }})
    }


    const onOpacityChange = ($event: React.FormEvent<HTMLInputElement>) => {
        dispatch({type: EditorActions.UpdateShape, payload:{ opacity: parseFloat($event.currentTarget.value) }})
    }

  return (
    <>
    
    Crop
    </>
  );
}
