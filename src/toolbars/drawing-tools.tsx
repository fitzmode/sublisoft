import React, { useContext, useState } from 'react';
import { AppContext, IAppState, IAction, EditorActions } from '../context/context';
import { PencilTypes } from '../models/models';
import { SketchPicker } from "react-color";
import Popover from 'react-tiny-popover'
import { Button } from "react-bootstrap";
import MaterialIcon from 'material-icons-react';


const pencils = [
  { name:'Vertical', key:PencilTypes.VerticalPattern , icon: 'border_vertical'},
  { name:'Cirle', key:PencilTypes.CirclePattern , icon: 'grain'},
  { name:'Horizontal', key:PencilTypes.HorizontalPattern , icon: 'border_horizontal'},
  { name:'Square', key:PencilTypes.SquarePencil , icon: 'view_comfy'},
  { name:'Default', key:PencilTypes.DefaultPencil , icon: 'border_color'},
]
export const DrawingTools = () => {
    const [{ selectedPencil }, dispatch] : [IAppState, React.Dispatch<IAction>] = useContext(AppContext) 
    const [{ open }, setState] = useState({ open: false})
  return (
    <>
      { pencils.map(pencil => (
          <Button 
          variant="link"
          key={pencil.key} onClick={() => {
            dispatch({ type: EditorActions.ChangePencilType, payload:{ type: pencil.key} })
            dispatch({type: EditorActions.UpdateState, payload:{ sidebarState: false }})
          }}>
          <MaterialIcon  icon={pencil.icon} size={24} color={"#fff"}/>
          </Button>
        ))}


<Popover
    isOpen={open}
    position={'bottom'} // preferred position
    onClickOutside={() => {
        setState(state => ({ 
            ...state,
            open: false }))
            
    }}
    content={({ position, targetRect, popoverRect }) => (
            <div style={{ background:'#373842', padding: 5}}>
          <SketchPicker color={selectedPencil.color} onChange={({ hex }) => {
    dispatch({ type: EditorActions.ChangePencilType, payload: {color: hex }})
  }} /> 
            </div>
    )}
>
    <Button 
    variant="link"
 
    onClick={() => {
 setState(state => ({ 
    ...state,
    open: !open }))}
    }
   >
        <MaterialIcon  icon={'color_lens'} size={24} color={selectedPencil.color}/>
    </Button>
</Popover>


<input type="number" min={0} max={50} step={1} value={selectedPencil.size} name="" id="" onChange={($event) => {
          dispatch({type: EditorActions.ChangePencilType, payload:{size: parseInt($event.currentTarget.value)}})
        }}/>

    </>
  );
}
