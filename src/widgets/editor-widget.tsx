import  React, {  useContext, useRef, useEffect } from 'react';
import { PageComponent } from "../components/page-component";
import { SidebarComponent } from '../components/sidebar-component';
import Sidebar from "react-sidebar";
import { AppContext, IAppState, IAction, EditorActions } from '../context/context';
import { ShapeTypes } from '../models/models';
import { defaultCirleProps, defaultSquareProps, defaultStarProps, defaultPolygonProps } from '../utils';
import { ImageTools } from '../toolbars/image-tools';
import { CanvasFooter } from '../components/footer';
import { Header } from '../components/header';
import { SettingsContextProvider } from '../context/settings-context';
interface IEditorProps {
}

const shapes = [
  {name: 'Circle', type: ShapeTypes.Circle, ...defaultCirleProps}, 
  {name:'Square', type: ShapeTypes.Square, ...defaultSquareProps},
  {name:'Star', type: ShapeTypes.Star, ...defaultStarProps},
  {name:'Polygon', type: ShapeTypes.Polygon, ...defaultPolygonProps},
]

const switchOptionPanel = (selected) => {
  switch (selected.type) {
    case ShapeTypes.Image:
      return <ImageTools />;
  
    default:
      break;
  }
}

export const EditorWidget: React.FunctionComponent<IEditorProps> = (props) => {

  const printRef = React.createRef();
  const stageRef:any = React.createRef();
  const [{ sidebarState, selectedPageSize }, dispatch]:[IAppState, React.Dispatch<IAction>] = useContext(AppContext);



  return (
    <div>
        <SettingsContextProvider>
        <div style={{}}>
       { selectedPageSize.key === EditorActions.PageSizeA4 ?  
        <PageComponent ref={{ printRef, stageRef}} /> : 
        <PageComponent ref={{printRef, stageRef}} />}
        </div>
        </SettingsContextProvider>

    </div>
  ) ;
};

 