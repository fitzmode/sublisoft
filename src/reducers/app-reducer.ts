
import shortid from 'shortid'

const zoomValues = Array.from(new Array(10), (x,i) => (i+1)*10);

const families =  ['Source Sans Pro:300,600', 'Karla', 'Modak'];
const fonts = families.map(font => font.substring(0, font.indexOf(':')) || font);

type PageSize = {
    name: string;
    key: string;
    width: number | string;
    height: number | string;
    orientation:string;
}
export interface IAppState {
    pageSizes: Array<PageSize>;
    selectedPageSize: PageSize;
    selectedOrientation: string;
    pageOrientations: Array<any>;
    zoomValues:Array<number>;
    selectedZoom:number;
    sidebarOptions:Array<any>
    shapes:Array<any>;
    selectedObject:any;
    presetImageSizes:Array<any>;
    selectedPresetImageSize:any;
    fonts:Array<any>;
    history:Array<any>;
    cropping: boolean;
    crop: any;
    mode: any,
    imageToPrint: string;
    historyStep:number;
    selectedPencil: {
        type: string;
        size: number;
        color:string;
    };
    sidebarState: boolean;
    textEditVisible: { 
        x?: number,
        y?: number;
        width?: number;
        height?: number;
        uid?: string;
    };
}

export enum EditorActions {
    ChangePageSize = 'CHANGE_PAGE_SIZE',
    ChangePageOrientation = 'CHANGE_PAGE_ORIENTATION',
    ChangeZoom = 'CHANGE_ZOOM',
    ChangeToolType = 'CHANGE_TOOL_TYPE',
    LandScapeOrientation = 'LANDSCAPE',
    PortraitOrientation = 'PORTRAIT',
    PageSizeA4 = 'PAGE_SIZE_A4',
    PageSizeA3 = 'PAGE_SIZE_A3',
    AddShape = 'CANVAS_ADD_SHAPE',
    UpdateShape = 'CANVAS_UPDATE_SHAPE',
    UpdateState = 'CANVAS_UPDATE_STATE',
    UpdateFontStyle = 'CANVAS_UPDATE_FONT_STYLE',
    SetSelected = 'SET_SELECTED_OBJECT',
    UpdateTextDecoration = 'CANVAS_UPDATE_TEXT_DECORATION',
    Duplicate = 'DUPE_SELECTED',
    FlipSelectedX = 'FLIP_SELECTED_X',
    FlipSelectedY = 'FLIP_SELECTED_Y',
    Delete = 'DELETE_SELECTED',
    Undo = 'UNDO_CHANGES',
    Redo = 'REDO_CHANGES',
    StateChanged = 'CANVAS_STATE_CHANGED',
    StartCrop = 'CANVAS_START_CROP',
    FinishCrop = 'CANVAS_FINISH_CROP',
    CancelCrop = 'CANVAS_CANCEL_CROP',
    UpdateCropSelection = 'UPDATE_CROP_SELECTION',
    StartDrawing = 'CANVAS_START_DRAWING',
    FinishDrawing = 'CANVAS_FINISH_DRAWING',
    CancelDrawing = 'CANVAS_CANCEL_DRAWING',
    ChangePencilType = 'CANVAS_SET_PENCIL',
    ChangePencilSize = 'CHANGE_PENCIL_SIZE',
    SendBack = 'SEND_BACK',
    BringForward = 'BRING_FORWARD',
    SetImageToPrint = 'SET_IMAGE_BEFORE_PRINT'
}




type AppProps = {

}

export interface IAction { payload?: any; type: EditorActions;};

const reducer = (state:IAppState, action: IAction ): IAppState => {
    switch (action.type) {
     
        case EditorActions.ChangePageSize:
            return { 
                ...state, 
                selectedPageSize: state
                .pageSizes
                .find(pageSize => pageSize.key === action.payload)
            }

        case EditorActions.ChangeZoom: 
            return {...state, selectedZoom: action.payload/100}

        case EditorActions.ChangeToolType: 
            return {...state, 
                mode: action.payload,
                // ...(action.payload === ShapeTypes.Pencil ? { mode: true } : { mode: false})
            }

        case EditorActions.ChangePencilType: 
        console.log(action.payload, "Selected pencil")
            return {...state, selectedPencil: {...state.selectedPencil, ...action.payload}}

        case EditorActions.ChangePageOrientation:
            return {...state, selectedOrientation: action.payload}
        case EditorActions.AddShape:
            return {...state, shapes: [...state.shapes, {...action.payload, uid: shortid.generate()}]}

        case EditorActions.SetSelected:
            return {...state, selectedObject: action.payload, mode: action.payload.type}

        case EditorActions.UpdateState: 
            return {...state, ...action.payload}
            
        case EditorActions.UpdateFontStyle:
            const fontStyle = state.selectedObject
            .fontStyle.includes(action.payload) ?  state.selectedObject.fontStyle.replace(action.payload,'') : `${state.selectedObject.fontStyle} ${action.payload}` 
            return {
                ...state, 
                selectedObject: {
                ...state.selectedObject,
                    fontStyle
            },
        shapes: state.shapes.map(shape => (shape.uid === state.selectedObject.uid ? 
            {...shape, fontStyle} : shape))}

        case EditorActions.UpdateShape:
            console.log("Updating shape", action.payload)
            return {...state, shapes: state.shapes.map(shape => (
                shape.uid === state.selectedObject.uid ?
                {...shape, ...action.payload} : shape
            )),
        selectedObject: {
            ...state.selectedObject,
            ...action.payload
        }}

        case EditorActions.SetImageToPrint: 
            return {
                ...state,
                imageToPrint: action.payload
            }
        case EditorActions.UpdateTextDecoration:
            const textDecoration = state.selectedObject
            .textDecoration.includes(action.payload) ?  state.selectedObject.textDecoration.replace(action.payload,'') : `${state.selectedObject.textDecoration} ${action.payload}` 
            return {
                ...state, 
                selectedObject: {
                ...state.selectedObject,
                textDecoration
            },
        shapes: state.shapes.map(shape => (shape.uid === state.selectedObject.uid ? 
            {...shape, textDecoration} : shape))}

        case EditorActions.UpdateShape:
            return {...state, shapes: state.shapes.map(shape => (
                shape.uid === state.selectedObject.uid ?
                {...shape, ...action.payload} : shape
            )),
        selectedObject: {
            ...state.selectedObject,
            ...action.payload
        }}

        case EditorActions.Duplicate:
            return {...state, shapes: [
                ...state.shapes,
                {
                   ...state.selectedObject,
                   uid: shortid.generate(),
                   x: state.selectedObject.x + 10,
                   y: state.selectedObject.y + 10
                }
            ]}

        case EditorActions.FlipSelectedX:
            return { ...state, 
                shapes: state.shapes.map(shape => (shape.uid === state.selectedObject.uid ? 
                {...shape, flipX: !shape.flipX, offsetX: shape.width/2} : shape) )}

        case EditorActions.FlipSelectedY:
            return { ...state, shapes: state.shapes.map(shape => (shape.uid === state.selectedObject.uid ? 
                {...shape, flipX: !shape.flipX} : shape) )}

        case EditorActions.Delete: 
            return { ...state, shapes: state.shapes.filter(shape => shape.uid !== state.selectedObject.uid)}

            case EditorActions.Undo:{
                if (state.historyStep === 0)  return state; 
                const undoStep = state.historyStep -= 1;
                return {
                    ...state,
                     shapes: state.history[undoStep],
                };}
    
            case EditorActions.Redo:{
             if (state.historyStep === state.history.length - 1) return state;
        const redoStep = state.historyStep += 1; 
            return {
                ...state,
                shapes:state.history[redoStep],
                historyStep: redoStep
            };
    }

   

    case EditorActions.StateChanged : {
        const nextStep = state.historyStep + 1; 
        return {
            ...state,
            history: [ ...state.history, state.shapes ],
            historyStep: nextStep
        }
    }

    case EditorActions.SendBack: {
        const sorted = state.shapes.sort((a, b) => a.zIndex - b.zIndex);
        const previous = state.shapes[sorted.findIndex(shape => shape.uid === state.selectedObject.uid) - 1];
        if(previous < 0) return state;
        const shapes = state.shapes.map(shape => (
            shape.uid === previous.uid ?
            {...shape, zIndex: shape.zIndex += 1  } : shape
            ))
            .map(shape => (
                shape.uid === state.selectedObject.uid ?
                {...shape, zIndex: shape.zIndex -= 1  } : shape
                ))
            return {
                ...state,
                shapes
            }
    }
  

        case EditorActions.BringForward: {
            const sorted = state.shapes.sort((a, b) => a.zIndex - b.zIndex);
            const next = state.shapes[sorted.findIndex(shape => shape.uid === state.selectedObject.uid) + 1];
            
            if(((next + 1) > state.shapes.length) || !next) return state;
            const shapes = state.shapes.map(shape => (
                shape.uid === next.uid ?
                {...shape, zIndex: shape.zIndex -=1  } : shape
                ))
                .map(shape => (
                    shape.uid === state.selectedObject.uid ?
                    {...shape, zIndex: shape.zIndex +=1  } : shape
                    ))
                return {
                    ...state,
                    shapes
                }
        }

    case EditorActions.StartCrop:
        return {...state, cropping: true}
    case EditorActions.CancelCrop:
        return {...state, cropping: false, crop: {}}
    case EditorActions.FinishCrop:
        return {
            ...state,
            cropping: false, 
            shapes: state.shapes.map(shape => (shape.uid === state.selectedObject.uid ? 
                {...shape, cropTo: {...state.crop}} : shape)),
            crop: {}}

    case EditorActions.UpdateCropSelection: 
        return { ...state, crop: action.payload}
        default:
            throw new Error(`Unhandled action: ${action.type}`);
    }
}