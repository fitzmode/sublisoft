import React from 'react';
import logo from './logo.svg';
import './App.css';
import { EditorWidget } from './widgets/editor-widget';
import { AppContextProvider } from './context/context';

const App = () => {
  return (
    <div className="App">
      <AppContextProvider>
        <EditorWidget/>
      </AppContextProvider>
    </div>
  );
}

export default App;
