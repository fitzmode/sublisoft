
import React, {useRef,useEffect} from 'react'
import { ShapeTypes } from '../models/models';

export default function useUpdateEffect(effect, dependencies = []) {
    const isInitialMount = useRef(true);
    useEffect(() => {
      if (isInitialMount.current) {
        isInitialMount.current = false;
      } else {
        effect();
      }
    }, dependencies);
  }

  export const defaultImageProps = {
    embossStrength: 0, 
    sepia:0, 
    grayScale:0, 
    blurRadius:0, 
    noise:0, 
    scaleY: 1, 
    scaleX:1, 
    offsetX:0,
    offsetY:0,
    flipX:false,
    flipY:false,
    fontStyle: null,
    x: 0, 
    y:0, 
    opacity: 1,
    cropTo: {
      x: null,
      y: null,
      width: null,
      height: null
    }
  }

  export const defaultCirleProps = {
    radius: 50,
    opacity:1,
    fill: 'red',
    stroke: '#000',
    strokeWidth: 4,
    x: 100, 
    y: 100,
  }

  export const defaultSquareProps = {
    width: 100,
    height: 100,
    x: 0, 
    y: 0,
    opacity: 1,
    fill: 'red',
    stroke: '#000',
    strokeWidth: 4
  }


  export const defaultStarProps = {
        numPoints: 6,
        innerRadius: 40,
        outerRadius: 70,
        fill: 'yellow',
        stroke: 'black',
        strokeWidth: 4,
        x: 100,
        y: 100
  }

  export const defaultPolygonProps = {
    x: 100,
    y: 150,
    sides: 6,
    radius: 70,
    fill: 'red',
    stroke: 'black',
    strokeWidth: 4
}

export const filters = [
  {name:'Blur', max: 50, step: 1, min: 0, value: 50, id:'blurRadius'},
  {name:'Noise', max: 20, step: 0.01, min: 0, value: 0, id:'noise'},
  {name:'Emboss', max: 1, step: 0.1, min: 0, value: 0, id:'embossStrength'},
  {name:'Sepia', max: 2, step: 0.01, min: 0, value: 0, id:'sepia'},
  {name:'Grayscale', max: 2, step: 0.01, min: 0, value: 0, id:'grayScale'},
]


export const text_props = [
  {name:'Font Size', max: 50, step: 0.05, min: 0, value: 0, id:'fontSize', icon: 'format_size'},
  {name:'Line Height', max: 50, step: 0.05, min: 0, value: 0, id:'lineHeight', icon: 'format_line_spacing'},
  {name:'Letter Spacing', max: 1, step: 0.1, min: 0, value:'0', id:'letterSpacing', icon: 'format_line_spacing'},
]


export const  align_props = [
  {name: 'L', id:'left', icon:'format_align_left'},
  {name: 'C', id:'center', icon:'format_align_center'},
  {name: 'R', id:'right', icon:'format_align_right'},
  {name: 'J', id:'justify',icon:'format_align_justify'}
]

export const font_styles = [
  {name: 'B', id:'bold', icon:"format_bold"},
  {name: 'I', id:'italic', icon:"format_italic"},
]

export const text_decoration = [
  {name: 'U', id:'underline', icon:"format_underline"},
  {name: '-', id:'line-through', icon:"format_strikethrough"},
]

export const imageShape =  {type: ShapeTypes.Image, name:"Image",embossStrength: 0, sepia:0, grayScale:0, blurRadius:0, noise:0, scaleY: 1, scaleX:1, x: 0, y:0, opacity: 1, url:'https://konvajs.org/assets/yoda.jpg' }

export const textShape = {type: ShapeTypes.Text, scaleY: 1, scaleX:1, color:'#ff4081', fontStyle: 'normal', name:'Text', textDecoration: '',letterSpacing: 0,visible: true, opacity: 1, lineHeight:1,  align:'left', width:null, height:null,  fontFamily: 'Karla', fontSize: 40, x: 0, y:0, text: 'Double Click to Edit' }