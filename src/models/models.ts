export enum ShapeTypes {
    Text = 'CANVAS_TEXT',
    Image = 'CANVAS_IMAGE',
    Pencil = 'CANVAS_PENCIL',
    Crop = 'CANVAS_CROP',
    Circle = 'CANVAS_CIRLCE',
    Square = 'CANVAS_SQUARE',
    Star = 'CANVAS_STAR',
    Polygon = 'CANVAS_POLYGON'
}

export enum PencilTypes {
    DefaultPencil = 'CANVAS_DEFAULT_PENCIL',
    CirclePencil = 'CANVAS_CIRCLE_PENCIL',
    CirclePattern = 'CANVAS_CIRCLE_PATTERN_PENCIL',
    HorizontalPattern = 'CANVAS_HORIZONTAL_PATTERN_PENCIL',
    VerticalPattern = 'CANVAS_VERTICAL_PATTERN_PENCIL',
    SprayPencil = 'CANVAS_SPRAY_PENCIL',
    DiamondPencil = 'CANVAS_DIAMOND_PENCIL', 
    SquarePencil = 'CANVAS_SQUARE_PENCIL'
}