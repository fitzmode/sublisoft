import React, { useContext, Fragment, useRef } from 'react';
import { IAppState, AppContext, EditorActions, IAction } from '../context/context';
import { ShapeTypes } from '../models/models';
import MaterialIcon, {colorPalette, } from 'material-icons-react';
import { Button } from "react-bootstrap";
import { imageShape, textShape, defaultImageProps } from '../utils';

export const SidebarComponent = () => {
  const [{cropping, selectedObject}, dispatch]: [IAppState, React.Dispatch<IAction>] = useContext(AppContext);


  const onFileChange = ($event) => {
    const {target} = $event;
    const {files} = target;

    if (files && files[0]) {
        const img = new Image();
        const url = URL.createObjectURL(files[0]);
        img.src = url;
        img.onload =  () => {
         dispatch({
            type: EditorActions.AddShape, 
            payload: { 
                type: ShapeTypes.Image, 
                url, 
                ...defaultImageProps,
                width: img.naturalWidth,
                height: img.naturalHeight
            }})

        };

         target.value = ''
    }
}

    const imagePicker = useRef(null);



  return (
    <div style={{width: 44,  height:"100%", position:'fixed', background: '#373842', left:0, zIndex:2, paddingTop: 80}}>

<input style={{position:"fixed", top: -100}} ref={imagePicker} type="file" name="" onInput={onFileChange} accept="image/*" id=""/>

    <Button 
    variant="link"
    onClick={() => { 
    //   dispatch({type: EditorActions.AddShape, payload: imageShape })
        imagePicker.current.click()
      }}>
        <MaterialIcon icon="add_photo_alternate" size={24} color='#fff'/>
    </Button>

    <Button 
    variant="link"
    onClick={() => { 
      dispatch({type: EditorActions.AddShape, payload: textShape })
      }}>
    <MaterialIcon icon="text_fields" size={24} color='#fff'/>
</Button>
            


             <Button 
    variant="link"
    onClick={() => { 
      dispatch({type: EditorActions.ChangeToolType, payload: ShapeTypes.Pencil })
      }}>
    <MaterialIcon icon="mode_edit" size={24} color='#fff'/>
</Button>


             {/* <div style={{height:80, display:'block', color:'#fff'}} onClick={() => { 
  dispatch({type: EditorActions.UpdateState, payload: { sidebarState: true}})
  }}>
                Shapes
             </div>  */}

             <Button 
    variant="link"
    onClick={() => dispatch({type: EditorActions.Delete})} >
    <MaterialIcon icon="delete_forever" size={24} color='#fff'/>
</Button>

    <Button 
        variant="link"
        onClick={() => dispatch({type: EditorActions.Undo})} >
        <MaterialIcon icon="undo" size={24} color='#fff'/>
    </Button>

    <Button 
        variant="link"
        onClick={() => dispatch({type: EditorActions.Redo})} >
        <MaterialIcon icon="redo" size={24} color='#fff'/>
    </Button>
 
     {!cropping && (
        <Button 
            variant="link"
            disabled={selectedObject.type !== ShapeTypes.Image}
            onClick={() => dispatch({type: EditorActions.StartCrop})} >
            <MaterialIcon icon="crop" size={24} color='#fff'/>
        </Button>
     )}

         <Button 
    variant="link"
    onClick={() => dispatch({type: EditorActions.BringForward})} >
    <MaterialIcon icon="flip_to_front" size={24} color='#fff'/>
</Button>    

<Button 
    variant="link"
    onClick={() => dispatch({type: EditorActions.SendBack})} >
    <MaterialIcon icon="flip_to_back" size={24} color='#fff'/>
</Button>
<Button 
    variant="link"
    onClick={() => dispatch({type: EditorActions.Duplicate})} >
    <MaterialIcon icon="content_copy" size={24} color='#fff'/>
</Button>

<Button 
    variant="link"
    onClick={() => dispatch({type: EditorActions.FlipSelectedX})} >
    <MaterialIcon icon="flip" size={24} color='#fff'/>
</Button>
<Button 
    variant="link"
    onClick={() => dispatch({type: EditorActions.FlipSelectedY})} >

        <span style={{transform:'rotate(90deg)', transformOrigin:'center center', display:'inline-block'}}>
        <MaterialIcon  icon="flip" size={24} color='#fff'/>

        </span>
</Button>



    </div>
  );
}
