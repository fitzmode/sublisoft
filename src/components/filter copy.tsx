import React, { useState } from "react";
import Popover from 'react-tiny-popover';
import { Button } from "react-bootstrap";
import { EditorActions } from "../context/context";
import Slider from 'react-input-slider';

export function FilterComponent({ selected, min, max,name, step,  key,  dispatch }) {

  const [{ open }, setState] = useState({ open: false })

  return (
    <Popover
    isOpen={open}
    position={'bottom'} 
    onClickOutside={() => {
        setState(state => ({ 
            ...state,
            open: false }))
    }}
    content={() => (
            <div style={{ background:'#373842',padding: 5, marginTop: 5, borderRadius: 2, boxSizing:'content-box'}}>

<Slider
  axis="x"
  x={selected[key]}
  xmin={min}
  xmax={max}
  xstep={step}
  onChange={({ x }) =>{ 
    dispatch({type: EditorActions.UpdateShape, 
        payload: { [key]: x }})
    }}
  styles={{
    track: {
      backgroundColor: '#FFFFFF',
      height: 2,
    },
    active: {
        backgroundColor: '#377BCA',
    },
    thumb: {
      width: 14,
      height: 14,
      backgroundColor: '#377BCA',
      boxShadow: '0 0 2px 2px #fff',
      cursor:'pointer'
    }
  }}
/>
</div>
    )}
>
    <Button 
    variant="link"
 
    onClick={() => {
 setState(state => ({ 
    ...state,
    open: !open }))}
    }
   >
       {name}
    </Button>
</Popover>
  );
}
