import  React, { useRef, useEffect, useState, useContext } from 'react';
import { Stage, Layer, Rect} from "react-konva";
import { AppContext, IAppState, EditorActions, IAction } from '../context/context';
import { ShapeTypes, PencilTypes } from '../models/models';
import { ImageComponent } from '../shapes/image';
import { TextComponent } from '../shapes/text';
import { CirclePattern } from '../pencils/circle-pattern';
import { HorizontalPattern } from '../pencils/horizontal-pattern';
import { VerticalPattern } from '../pencils/vertical-pattern';
import { CircleShape } from '../shapes/circle';
import { SquareShape } from '../shapes/square';
import { StarShape } from '../shapes/star';
import { PolygonShape } from '../shapes/polygon';
import { SquarePattern } from '../pencils/square-pattern';
import { DefaultBrush } from '../pencils/default';
import Portal from './portal';
import 'react-image-crop-component/style.css';
import { Header } from './header';
import { SidebarComponent } from './sidebar-component';
import { ISettingsState, SettingsContext } from '../context/settings-context';
import { TransfromerComponent } from '../shapes/handler';

if (process.env.NODE_ENV === 'development') {
    const whyDidYouRender = require('@welldone-software/why-did-you-render');
    whyDidYouRender(React, {
      trackAllPureComponents: true,
    });
  }

interface IAppProps {
    ref: any;
}



const renderShape = (shape, dispatch, textEditVisible, selectedObject, cropping) => {
    switch (shape.type) {
        case ShapeTypes.Image:
            return <ImageComponent key={shape.uid} shape={shape} dispatch={dispatch} cropping={cropping}/>;
        case ShapeTypes.Text:
            return <TextComponent key={shape.uid} shape={shape} dispatch={dispatch} selectedObject={selectedObject} textEditVisible={textEditVisible}/>
        case ShapeTypes.Circle:
            return <CircleShape key={shape.uid} shape={shape} dispatch={dispatch}/>
        case ShapeTypes.Square:
            return <SquareShape key={shape.uid} shape={shape} dispatch={dispatch}/>
        case ShapeTypes.Star:
            return <StarShape key={shape.uid} shape={shape} dispatch={dispatch}/>
        case ShapeTypes.Polygon:
            return <PolygonShape key={shape.uid} shape={shape} dispatch={dispatch}/>
        case PencilTypes.VerticalPattern:
            return <VerticalPattern key={shape.uid} {...shape} />
        case PencilTypes.HorizontalPattern:
            return <HorizontalPattern key={shape.uid} {...shape}/>;
        case PencilTypes.SquarePencil:
            return <SquarePattern key={shape.uid} {...shape}/>;
        case PencilTypes.CirclePattern:
            return <CirclePattern key={shape.uid} {...shape}/>;
        case PencilTypes.DefaultPencil:
            return <DefaultBrush key={shape.uid} {...shape}/>;
        default:
            return null;
    }
}

export const PageComponent: React.FunctionComponent<IAppProps> = React.forwardRef((props, refs:any) => {

    const container = useRef(null);
    // const layer = useRef(null);

    const { printRef, stageRef} = refs
    // const image = useRef(null);

 
      
 



    const [{
             shapes, 
             selectedObject, 
             selectedPageSize, 
             selectedPencil,
             cropping,
             mode,
             textEditVisible,
             imageToPrint,
             selectedOrientation } , dispatch]: [IAppState, React.Dispatch<IAction>] = useContext(AppContext);

             const [{selectedZoom
} , dispatchSettings]: [ISettingsState, React.Dispatch<any>] = useContext(SettingsContext);


      

    const [{ width, lines, drawing, height, src}, setState] = useState({ 
        width: 0,
        drawing:false,
         height: 0, 
         src: '',
      lines:[]});

      
         useEffect(() => {

            // stageRef.current.x((width - selectedZoom * width)/2)
        }, [selectedZoom])

          const stageMouseDown = () => {
                if(mode === ShapeTypes.Pencil) {
                    setState(state => ({
                        ...state,
                        drawing: true,
                        lines: []
                        }));
                }
          }


          const onCropped =  async ($event) => {
            const { image, data } = $event;
            const {x, y, width, height} = data
            const response = await fetch(image);
            const blob = await response.blob()
            const url = window.URL.createObjectURL(blob);
            dispatch({type: EditorActions.UpdateState, payload: {
                crop: {
                    url,
                    x: selectedObject.x + Math.abs(x) ,
                    y: selectedObject.y + Math.abs(y),
                    width,
                    height
                }
            }})

        }

          const stageMouseMove = () => {
                if(mode === ShapeTypes.Pencil) {
                  if (!drawing) {
                      return;
                    }
                    const point = stageRef.current.getStage().getPointerPosition();
                    setState(state => ({
                      ...state,
                      lines: [...lines,point.x, point.y]
                    }));
                }
          }


    useEffect(() => {
        if(container.current) {
            setState(state => ({
                ...state,
                width: container.current.getBoundingClientRect().width,
                height: container.current.getBoundingClientRect().height,
                loaded: true
            }))
        }
    }, [container, selectedPageSize, selectedOrientation])
    
 
  return (
   
      <div style={{background:'#1E2024', position:'relative' }}>
         



{/* 
  // @ts-ignore */}
<Header ref={{printRef, stageRef}}/>
<SidebarComponent/>


<div style={{paddingTop: 70, display:'flex', justifyContent:'center', paddingBottom: 70}}>

<Stage onMouseDown={stageMouseDown}
                 onMouseMove={stageMouseMove}
                 x={selectedZoom < 1 ? (width - (width * selectedZoom)) / 2 : 0}
                 scale={{x: selectedZoom, y: selectedZoom}}
                  onContentMouseup={() => {
                    if(mode) {
                        setState(state => ({ ...state, drawing: false}));
                        dispatch({ type: EditorActions.AddShape, payload: { lines, ...selectedPencil}})
                        setState(state => ({ ...state,lines:[] }));
                    }
                  }}
                 ref={stageRef} {...(selectedOrientation === EditorActions.PortraitOrientation ? { width, height} : {height: width, width: height})}>
                     <Layer>
                     <Rect fill="#fff" 
                    {...(selectedOrientation === EditorActions.PortraitOrientation ? { width, height} : {height: width, width: height})}
                     />
                     </Layer>
                <Layer>
                    { shapes.map((shape, idx) => (
                            renderShape(shape, dispatch, textEditVisible, selectedObject, cropping)
                        ))}
                       {/* { (!cropping && selectedObject.uid ) && (
                            <Handler {...selectedObject}/>
                        )} */}

                         { 
                           (mode === ShapeTypes.Pencil) && renderDrawing(selectedPencil,lines)
                        } 
                {(textEditVisible.uid && (selectedObject.type === ShapeTypes.Text))  && ( 
                <Portal>
                            <textarea
                            style={{
                            position: 'absolute',
                            left: textEditVisible?.x,
                            top: textEditVisible?.y,
                            width: selectedObject.width,
                            height: selectedObject.height,
                            fontFamily: selectedObject.fontFamily,
                            fontSize: selectedObject.fontSize,
                            outline: 'none',
                            lineHeight: selectedObject.lineHeight,
                            resize: 'none',
                            border: '1px solid blue',
                            color: selectedObject.color,
                            textAlign: selectedObject.align,
                            transform: `rotateZ(${selectedObject.rotation}deg)`,
                            overflow: 'hidden'
                        }}
                        value={selectedObject.text} // innerHTML of the editable di
                        disabled={false}       // use true to disable editing
                        onChange={($event ) => {
                            dispatch({type: EditorActions.UpdateShape, payload: { text: $event.currentTarget.value }})

                        }}
                        onKeyDown={($event) => {
                            dispatch({type: EditorActions.UpdateShape, payload: { height: 'auto'}})
                            dispatch({type: EditorActions.UpdateShape, payload: { height: $event.currentTarget.scrollHeight + selectedObject.fontSize}})
                        }}

                        onBlur={($event ) => {
                            dispatch({type: EditorActions.UpdateState, payload: { textEditVisible: {}}})
                            dispatch({type: EditorActions.UpdateShape})
                        }}
                        />
                                </Portal>)} 
                                {(!cropping && selectedObject.uid) && <TransfromerComponent 
                                    id={selectedObject.uid}/>}
                </Layer>
            </Stage>
</div>
  
         

            <div ref={container} style={{width: selectedPageSize.width, height: selectedPageSize.height, position:'fixed', visibility: 'hidden', top: -1000}}> 
            </div>  

            <img id={switchPrintId(selectedPageSize, selectedOrientation)} ref={printRef} src={imageToPrint} alt=""
            // style={{position:'fixed', top: -1000 }}
            style={{...(
                selectedOrientation === EditorActions.PortraitOrientation ? 
                { width, height} : {height: width, width: height}
                )}}
            />
{/* 
<img id={switchPrintId(selectedPageSize, selectedOrientation)} ref={image2} className="image2" src={src} alt=""
            // style={{position:'fixed', top: 1000 }}
            style={{...(
                selectedOrientation === EditorActions.PortraitOrientation ? 
                { width, height} : {height: width, width: height}
                )}}
            /> */}
            </div>

  ) ;
});


const renderDrawing = (pencil, lines) => {
    switch (pencil.type) {
        case PencilTypes.VerticalPattern:
            return <VerticalPattern {...pencil} lines={lines}/>;
        case PencilTypes.HorizontalPattern:
            return <HorizontalPattern {...pencil} lines={lines}/>;
        case PencilTypes.SquarePencil:
            return <SquarePattern {...pencil} lines={lines}/>;
        case PencilTypes.DefaultPencil:
            return <DefaultBrush  {...pencil} lines={lines}/>;
        case PencilTypes.CirclePattern:
            return <CirclePattern  {...pencil} lines={lines}/>;
        default:
            break;
    }
}

const switchPrintId = (selectedPageSize, selectedOrientation) => {
    switch (true) {
        case ((selectedOrientation === EditorActions.PortraitOrientation) 
        && (selectedPageSize.key === EditorActions.PageSizeA4)) :
        
        return `${EditorActions.PortraitOrientation}_${EditorActions.PageSizeA4}`

        case ((selectedOrientation === EditorActions.PortraitOrientation) 
        && (selectedPageSize.key === EditorActions.PageSizeA3)) :

        return `${EditorActions.PortraitOrientation}_${EditorActions.PageSizeA3}`
        case ((selectedOrientation === EditorActions.LandScapeOrientation) 
        && (selectedPageSize.key === EditorActions.PageSizeA4)) :
        return `${EditorActions.LandScapeOrientation}_${EditorActions.PageSizeA4}`
        case ((selectedOrientation === EditorActions.LandScapeOrientation) 
        && (selectedPageSize.key === EditorActions.PageSizeA3)) :
        return `${EditorActions.LandScapeOrientation}_${EditorActions.PageSizeA3}`


        default:
            return `${EditorActions.PortraitOrientation}_${EditorActions.PageSizeA4}`
        }
}


PageComponent['whyDidYouRender'] = true






