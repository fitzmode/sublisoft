import React, {} from 'react';
import { Group, Rect, Transformer } from "react-konva";

export const CustomCropper = ({ condition, children }) => {
  return (
   condition ?
    <Group>
        { children }
        <Rect fill="red" width={20} height={20} />
    </Group> 
   : children
  );
}
