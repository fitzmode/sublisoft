import React, { useState } from "react";
import Popover from 'react-tiny-popover';
import { Button } from "react-bootstrap";
import { EditorActions } from "../context/context";
import Slider from 'react-input-slider'
import MaterialIcon from 'material-icons-react';




type FilterProps = {
    selected?:any;
     min?:any; 
     max?:any;
     name?:any;
     icon?:any; 
     id?:any; 
     step?:any; 
     dispatch?:any;
}
export const  FilterComponent: React.FunctionComponent<FilterProps> = ({ selected, min, max,name, icon, step,  id,  dispatch }) => {
  const [{ open }, setState] = useState({ open: false })

  return (
    <Popover
    isOpen={open}
    position={'bottom'} 
    onClickOutside={() => {
        setState(state => ({ 
            ...state,
            open: false }))
    }}
    content={() => (
            <div style={{ background:'#373842',padding: 5, marginTop: 5, borderRadius: 2}}>

<Slider
  axis="x"
  x={selected[id]}
  xmin={min}
  xmax={max}
  xstep={step}
  onChange={({ x }) =>{ 
    dispatch({type: EditorActions.UpdateShape, 
        payload: { [id]: x }})
    }}
  styles={{
    track: {
      // backgroundColor: '#FFFFFF',
      // height: 2,
    },
    active: {
        backgroundColor: '#377BCA',
    },
    thumb: {
      // width: 14,
      // height: 14,
      // backgroundColor: '#377BCA',
      // boxShadow: '0 0 2px 2px #fff',
      // cursor:'pointer'
    }
  }}
/>
</div>
    )}
>
  <Button 
    variant="link"
    style={{transform: id === 'letterSpacing' ? 'rotate(-90deg)' : ''}}
    onClick={() => {
    setState(state => ({ 
        ...state,
        open: !open }))}
        }> 



        {
          icon ?  <MaterialIcon icon={icon} size={24} color='#fff'/> 
               : name
        }
   
      
    </Button>
</Popover>
);
      }
