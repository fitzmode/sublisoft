import React, { useContext, Fragment, useRef, useState } from 'react';
import { EditorActions, IAppState, IAction, AppContext } 
from '../context/context';
import { ShapeTypes } from '../models/models';
import { DrawingTools } from '../toolbars/drawing-tools';
import { TextTools } from '../toolbars/text-tools';
import MaterialIcon, {} from 'material-icons-react';
import { Button } from "react-bootstrap";
import { ImageTools } from '../toolbars/image-tools';
import { CropTools } from '../toolbars/crop-tools';
import 'react-dropdown/style.css'
import ReactToPrint from 'react-to-print'
import { SettingsContext, ISettingsState, SettingsActions } from '../context/settings-context';
import Slider from 'react-input-slider';
import Popover from 'react-tiny-popover';

 


export const Header = React.forwardRef(({}, refs:any) => {

    const { printRef, stageRef} = refs;
    const image_15_x_15 = useRef(null);
    const [{ open }, setState] = useState({ open: false })


    const [{selectedZoom}, dispatchSettings]: [ISettingsState, React.Dispatch<any>] = useContext(SettingsContext)

    

const [{mode, pageSizes, cropping, selectedObject, selectedPresetImageSize, presetImageSizes, selectedOrientation, pageOrientations,selectedPageSize}, dispatch]: [IAppState, React.Dispatch<IAction>] = useContext(AppContext)
const onPageSizeChange = ($event: React.FormEvent<HTMLSelectElement>) => {
    dispatch({type: EditorActions.ChangePageSize, payload: $event.currentTarget.value})
}



const onOrientationChange = ($event: React.FormEvent<HTMLSelectElement>) => {
    dispatch({type: EditorActions.ChangePageOrientation, payload: $event.currentTarget.value})
}

const onPresetImageSizeChange = ($event: React.FormEvent<HTMLSelectElement>) => {
    dispatch({type: EditorActions.UpdateShape, payload:{
                width: image_15_x_15.current.getBoundingClientRect().width,
                height: image_15_x_15.current.getBoundingClientRect().height,
        }
    })
}


const saveImage = () => {
    const dataURL = stageRef.current.toDataURL();
    downloadURI(dataURL, 'stage.png');
  }
  return (
    <div style={{background:'#373842', position:'fixed', top: 0, left: 0, width:'100%', zIndex: 100, height: 50, paddingLeft: 70}}>
    { mode === ShapeTypes.Text  && <TextTools />}
    { mode === ShapeTypes.Image  && <ImageTools />}
    { mode === ShapeTypes.Pencil  && <DrawingTools />}
    { mode === ShapeTypes.Crop  && <CropTools />}

 
         <select disabled={selectedObject.type !== ShapeTypes.Image} value={selectedPresetImageSize.key} onChange={onPresetImageSizeChange} name="" id="">
             { presetImageSizes.map(size => (
                     <option key={size.key} value={size.key}>
                         {size.name}
                     </option>
                 ))}
         </select> 
         <select name="" id="" value={selectedOrientation} onChange={onOrientationChange}>
             {
                 pageOrientations.map(orientation => (
                     <option key={orientation} value={orientation}>{orientation}</option>
                 ))
             }
         </select> 
            <select value={selectedPageSize.key} onChange={onPageSizeChange}>
             { pageSizes.map(pageSize => (
                     <option key={pageSize.key} value={pageSize.key}>{pageSize.name}</option>
                 ))}
     </select>

 
 
 

       
  {
      (cropping) && (<>
      <Button 
        variant="link"
        onClick={() => {
            dispatch({type: EditorActions.FinishCrop})
            dispatch({type: EditorActions.StateChanged})
        }}>
        Crop
      </Button> 
      <Button 
        variant="link"
        onClick={() => {
            dispatch({type: EditorActions.CancelCrop})
            dispatch({type: EditorActions.StateChanged})

        }}>
        Cancel
      </Button> 
       </>)
  }



<Button 
    variant="link"
    onClick={saveImage} >
    <MaterialIcon icon="save" size={24} color='#fff'/>
</Button>

    <ReactToPrint
                 trigger={() => <Button variant="link">
                    <MaterialIcon icon="print" size={24} color='#fff'/>
                </Button>}
                onBeforeGetContent={() => {
                    return new Promise((resolve, reject) => {
                        const payload = stageRef.current.toDataURL();
                        dispatch({type: EditorActions.SetImageToPrint, payload})
                        resolve();
                    })
                   
                }}
                 content={() => printRef.current}
                 />


    
<Popover
    isOpen={open}
    position={'bottom'} 
    onClickOutside={() => {
        setState(state => ({ 
            ...state,
            open: false }))
    }}
    content={() => (
            <div style={{ background:'#373842',padding: 5, marginTop: 5, borderRadius: 2}}>

<Slider
  axis="x"
  x={selectedZoom}
  xmin={0.1}
  xmax={1}
  xstep={0.01}
  onChange={({ x }) =>{ 
    dispatchSettings({ type: SettingsActions.ChangeZoom, payload: x})
    }}
  styles={{
    track: {
      backgroundColor: '#FFFFFF',
      height: 2
    },
    active: {
        backgroundColor: '#377BCA',
    },
    thumb: {
      width: 14,
      height: 14,
      backgroundColor: '#377BCA',
      boxShadow: '0 0 2px 2px #fff',
      cursor:'pointer'
    }
  }}
/>
</div>
    )}>
  <Button 
    variant="link"
    onClick={() => {
    setState(state => ({ 
        ...state,
        open: !open }))}
        }
        > 
        <MaterialIcon icon="zoom_in" size={28} color='#fff'/>
    </Button>
</Popover>



<div ref={image_15_x_15} style={{width: '15cm', height:'15cm', position:'fixed', top: -1000}}></div>
 </div>
  );
})


const  downloadURI = (uri, name) => {
    var link = document.createElement('a');
    link.download = name;
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    // delete link;
  }