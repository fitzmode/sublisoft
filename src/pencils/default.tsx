import React from 'react';
import { Shape } from "react-konva";


export const DefaultBrush = ({ lines, size, color }) => {
    return (
        <Shape
            sceneFunc={(context:any, shape) => {
              context.beginPath();
              context.strokeStyle = shape.getAttr('patternFill');
              context.lineJoin = "round";
              context.lineCap = "round";
              const _points = shape.getAttr("points");
              context.moveTo(_points[0], _points[1]);
              for (let n = 2; n < _points.length; n += 2) {
                context.lineTo(_points[n], _points[n + 1]);
              }
              context.lineWidth = shape.getAttr("strokeWidth") || 10;
              context.stroke();
              // (!) Konva specific method, it is very important
              context.fillStrokeShape(shape);
              context.closePath();
            }}
            // listening={true}
            strokeWidth={size}
            stroke={"transparent"}
            draggable
            patternFill={color}
            points={lines}
          />
       
    )
}