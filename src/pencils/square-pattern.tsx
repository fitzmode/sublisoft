import React, { Fragment, useMemo } from 'react';
import { Shape } from "react-konva";


export const SquarePattern = ({ lines, size, color }) => {
    const memoizedCanvas = useMemo(() => {
        const patternCanvas = document.createElement("canvas");
        return patternCanvas;
      }, []);

    return (
        <Shape
            sceneFunc={(context:any, shape) => {
              const squareWidth = 10;
              const squareDistance = 2;
              const patternCtx = memoizedCanvas.getContext("2d");
              memoizedCanvas.width = memoizedCanvas.height = squareWidth + squareDistance;
              patternCtx.beginPath();
              patternCtx.fillStyle = shape.getAttr('patternFill');
              patternCtx.fillRect(0, 0, squareWidth, squareWidth);
              patternCtx.closePath();
              const strokePattern = context.createPattern(
                memoizedCanvas,
                "repeat"
              );
             
              context.beginPath();
              context.strokeStyle = strokePattern;
              context.lineJoin = "round";
              context.lineCap = "round";
              const _points = shape.getAttr("points");
              context.moveTo(_points[0], _points[1]);
              for (let n = 2; n < _points.length; n += 2) {
                context.lineTo(_points[n], _points[n + 1]);
              }

            
              context.lineWidth = shape.getAttr("strokeWidth") || 10;
              context.stroke();
              // (!) Konva specific method, it is very important
              context.fillStrokeShape(shape);
              context.closePath();
            }}
            strokeWidth={size}
            stroke={"transparent"}
            patternFill={color}
            points={lines}
          />
       
    )
}