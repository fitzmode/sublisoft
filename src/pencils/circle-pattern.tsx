import React, { Fragment, useMemo } from 'react';
import { Shape } from "react-konva";


export const CirclePattern = ({ lines, size, color }) => {
    const memoizedCanvas = useMemo(() => {
        const patternCanvas = document.createElement("canvas");
        return patternCanvas;
      }, []);

    return (
        <Shape
            sceneFunc={(context:any, shape) => {
              const dotWidth = 20;
              const dotDistance = 5;
              const patternCtx = memoizedCanvas.getContext("2d");
              memoizedCanvas.width = memoizedCanvas.height = dotWidth + dotDistance;
              patternCtx.beginPath();
              patternCtx.fillStyle = shape.getAttr('patternFill');
              patternCtx.arc(dotWidth / 2, dotWidth / 2, dotWidth / 2, 0, Math.PI * 2, false);
              patternCtx.closePath();
              patternCtx.fill();
              const strokePattern = context.createPattern(
                memoizedCanvas,
                "repeat"
              );
              // const midPointBtw = (p1, p2) => {
              //   return {
              //     x: p1.x + (p2.x - p1.x) / 2,
              //     y: p1.y + (p2.y - p1.y) / 2
              //   };
              // };

              context.beginPath();
              context.strokeStyle = strokePattern;
              context.lineJoin = "round";
              context.lineCap = "round";
              const _points = shape.getAttr("points");
              context.moveTo(_points[0], _points[1]);
              for (let n = 2; n < _points.length; n += 2) {
                context.lineTo(_points[n], _points[n + 1]);
              }

            
              context.lineWidth = shape.getAttr("strokeWidth") || 10;
              context.stroke();
              // (!) Konva specific method, it is very important
              context.fillStrokeShape(shape);
              context.closePath();
            }}
            // listening={true}
            strokeWidth={size}
            stroke={"transparent"}
            patternFill={color}
            points={lines}
          />
       
    )
}