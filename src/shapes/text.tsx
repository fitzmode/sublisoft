import React, { useEffect, useRef } from 'react';
import { Text } from "react-konva";
import { EditorActions, IAction } from '../context/context';
import Konva from 'konva';

type ShapeProps = {
  dispatch: React.Dispatch<IAction>,
  shape:any;
  textEditVisible?: any;
  selectedObject:any;
}

export const TextComponent: React.FunctionComponent<ShapeProps> = ({ shape, dispatch ,textEditVisible, selectedObject}) => {
  const text = useRef(null)
  useEffect(() => {
    dispatch({ type: EditorActions.StateChanged })

    return () => {
    dispatch({ type: EditorActions.StateChanged })
    }
}, [])

useEffect(() => {
  if(text.current) {
    const layer = text.current.getLayer();
    const stage = layer.getStage();
    const textNode = text.current;
  
    // layer.add(textNode);
  
    const tr = new Konva.Transformer({
      // @ts-ignore
      node: textNode,
      enabledAnchors: ['middle-left', 'middle-right'],
      // set minimum width of text
      boundBoxFunc: function(oldBox, newBox) {
        newBox.width = Math.max(30, newBox.width);
        return newBox;
      }
    });
  
    textNode.on('transform', function() {
      // reset scale, so only with is changing by transformer
      textNode.setAttrs({
        width: textNode.width() * textNode.scaleX(),
        scaleX: 1
      });
    });
  
    layer.add(tr);
  
    layer.draw();
  
    textNode.on('dblclick', () => {
      // hide text node and transformer:
      textNode.hide();
      tr.hide();
      layer.draw();
  
      // create textarea over canvas with absolute position
      // first we need to find position for textarea
      // how to find it?
  
      // at first lets find position of text node relative to the stage:
      // @ts-ignore
      const textPosition = textNode.absolutePosition();
  
      // then lets find position of stage container on the page:
      const stageBox = stage.container().getBoundingClientRect();
  
      // so position of textarea will be the sum of positions above:
      const areaPosition = {
        x: stageBox.left + textPosition.x,
        y: stageBox.top + textPosition.y
      };
  
      // create textarea and style it
      const textarea = document.createElement('textarea');
      document.body.appendChild(textarea);
  
      // apply many styles to match text on canvas as close as possible
      // remember that text rendering on canvas and on the textarea can be different
      // and sometimes it is hard to make it 100% the same. But we will try...
      textarea.value = textNode.text();
      textarea.style.position = 'absolute';
      textarea.style.top = areaPosition.y + 'px';
      textarea.style.left = areaPosition.x + 'px';
      textarea.style.width = textNode.width() - textNode.padding() * 2 + 'px';
      textarea.style.height =
        textNode.height() - textNode.padding() * 2 + 5 + 'px';
      textarea.style.fontSize = textNode.fontSize() + 'px';
      textarea.style.fontWeight = textNode.fontStyle();
      textarea.style.border = 'none';
      textarea.style.padding = '0px';
      textarea.style.margin = '0px';
      textarea.style.overflow = 'hidden';
      textarea.style.background = 'none';
      textarea.style.outline = 'none';
      textarea.style.resize = 'none';
      // @ts-ignore
      textarea.style.lineHeight = textNode.lineHeight();
      textarea.style.fontFamily = textNode.fontFamily();
      textarea.style.transformOrigin = 'left top';
      textarea.style.textAlign = textNode.align();
      textarea.style.color = textNode.fill();
      const rotation = textNode.rotation();
      var transform = '';
      if (rotation) {
        transform += 'rotateZ(' + rotation + 'deg)';
      }
  
      var px = 0;
      // also we need to slightly move textarea on firefox
      // because it jumps a bit
      var isFirefox =
        navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
      if (isFirefox) {
        px += 2 + Math.round(textNode.fontSize() / 20);
      }
      transform += 'translateY(-' + px + 'px)';
  
      textarea.style.transform = transform;
  
      // reset height
      textarea.style.height = 'auto';
      // after browsers resized it we can set actual value
      textarea.style.height = textarea.scrollHeight + 3 + 'px';
  
      textarea.focus();
  
      function removeTextarea() {
        textarea.parentNode.removeChild(textarea);
        window.removeEventListener('click', handleOutsideClick);
        textNode.show();
        tr.show();
        tr.forceUpdate();
        layer.draw();
      }
  
      function setTextareaWidth(newWidth) {
        if (!newWidth) {
          // set width for placeholder
  // @ts-ignore
          newWidth = textNode.placeholder.length * textNode.fontSize();
        }
        // some extra fixes on different browsers
        var isSafari = /^((?!chrome|android).)*safari/i.test(
          navigator.userAgent
        );
        var isFirefox =
          navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
        if (isSafari || isFirefox) {
          newWidth = Math.ceil(newWidth);
        }
  // @ts-ignore
        var isEdge = document.documentMode || /Edge/.test(navigator.userAgent);
        if (isEdge) {
          newWidth += 1;
        }
        textarea.style.width = newWidth + 'px';
      }
  
      textarea.addEventListener('keydown', function(e) {
        // hide on enter
        // but don't hide on shift + enter
        if (e.keyCode === 13 && !e.shiftKey) {
          textNode.text(textarea.value);
          removeTextarea();
        }
        // on esc do not set value back to node
        if (e.keyCode === 27) {
          removeTextarea();
        }
      });
  
      textarea.addEventListener('keydown', (e)  => {
        const scale = textNode.getAbsoluteScale().x;
        setTextareaWidth(textNode.width() * scale);
        textarea.style.height = 'auto';
        textarea.style.height =
          textarea.scrollHeight + textNode.fontSize() + 'px';
      });
  
      function handleOutsideClick(e) {
        if (e.target !== textarea) {
          textNode.text(textarea.value);
          removeTextarea();
        }
      }
      setTimeout(() => {
        window.addEventListener('click', handleOutsideClick);
      });
    });
  }
}, [text])




const handleTextDblClick = ($event) => {
  const textPosition = $event.target.getAbsolutePosition();
  const stageBox = $event.target.getStage().container().getBoundingClientRect();
  dispatch({
    type: EditorActions.UpdateState,
    payload: {textEditVisible: {
      x: textPosition.x + stageBox.left,
      y: textPosition.y + stageBox.top,
      width: $event.target.width(),
      height: $event.target.height(),
      uid: shape.uid
    },
 }
  });
  dispatch({type: EditorActions.UpdateShape, payload: { visible: false}})

};

  return (
      <Text 
    ref={text}
    id={shape.uid}
    onMouseDown={() => dispatch({type: EditorActions.SetSelected, payload: shape})} 
    onDragEnd={() => { dispatch({type: EditorActions.UpdateShape, payload: { x: text.current.x(), y: text.current.y()}})}}
    x={shape.x} 
    
    // filters={[Konva.Filters.Blur, Konva.Filters.Sepia, Konva.Filters.Grayscale, Konva.Filters.Brighten, Konva.Filters.Contrast, Konva.Filters.Emboss]} 
    offset={{x: shape.width/2, y: shape.height/2}} 
    scale={{x: shape.scaleX, y: shape.scaleY}} 
    textDecoration={shape.textDecoration}  
    opacity={shape.opacity} 
    fontStyle={shape.fontStyle} 
    letterSpacing={shape.letterSpacing} 
    width={shape.width} height={shape.height}  
    lineHeight={shape.lineHeight}   
    // blurRadius={shape.blurRadius}
    fill={shape.color}
    y={shape.y} 
    align={shape.align} 
    text={shape.text} 
    fontSize={shape.fontSize} 
    zIndex={shape.zIndex && shape.zIndex}
    visible={shape.visible}
    fontFamily={shape.fontFamily} draggable/> 
  );
}
