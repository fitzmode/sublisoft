import React, { useEffect, useRef } from 'react';
import { Star } from "react-konva";
import { EditorActions, IAction } from '../context/context';
import Konva from 'konva';

type ShapeProps = {
  dispatch: React.Dispatch<IAction>,
  shape:any;
}

export const StarShape: React.FunctionComponent<ShapeProps> = ({ shape, dispatch }) => {

  useEffect(() => {
    dispatch({ type: EditorActions.StateChanged })

    return () => {
    dispatch({ type: EditorActions.StateChanged })
    }
}, [])

  return (
    <Star 
    id={shape.uid}
    x={shape.x} 
    y={shape.y} 
    innerRadius={shape.innerRadius}
    outerRadius={shape.outerRadius}
    numPoints={shape.numPoints}
    fill={shape.fill}
    scale={{x: shape.scaleX, y: shape.scaleY}} 
    opacity={shape.opacity} 
    stroke={shape.stroke}
    zIndex={shape.zIndex && shape.zIndex}
    strokeWidth={shape.strokeWidth}
    draggable/>
  );
}
