import React, { useRef, useEffect, useCallback, Fragment } from 'react';
import { Image, Group, Transformer, Rect } from 'react-konva';
import useImage from 'use-image';
import { EditorActions, IAction } from '../context/context';
import Konva from 'konva';
import useUpdateEffect from '../utils';
import { CustomCropper } from "../components/custom";


type IShapeProps = {
  shape:any;
  cropping: boolean;
  dispatch: React.Dispatch<IAction>
}

 export const OriginalImageComponent: React.FunctionComponent<IShapeProps> = ({ shape, dispatch, cropping }) => {
  const [image] = useImage(shape.url,'Anonymous');
  const imageRef = useRef(null);




  useEffect(() => {
    dispatch({ type: EditorActions.StateChanged })

    return () => {
    dispatch({ type: EditorActions.StateChanged })
    }
}, [])




  useEffect(() => {
    if(!image) return;
    const layer = imageRef.current.getLayer();
    imageRef.current.cache()
    layer.batchDraw()
  }, [shape])

  const updateShape = ({ currentTarget }) => {
    dispatch({
      type: EditorActions.UpdateShape, 
      payload: { 
        // scaleX: currentTarget.scaleX(), 
        //          scaleY: currentTarget.scaleY(),
                 x: currentTarget.x(),
                 y: currentTarget.y()
                }
    })
    dispatch({ type: EditorActions.StateChanged })
  }

  const updateSelected = () => {
    dispatch({type: EditorActions.SetSelected, payload: shape})
  }




  return (

<>
    <Image 
      id={shape.uid}
      ref={imageRef}
      image={image}
      x={shape.x}
      y={shape.y}
      width={shape.width}
      height={shape.height}
      onMouseDown={updateSelected} 
      // onDragEnd={updateShape}
      onDragEnd={() => {
        dispatch({
          type: EditorActions.UpdateShape, 
          payload: { 
            // scaleX: currentTarget.scaleX(), 
            //          scaleY: currentTarget.scaleY(),
                     x: imageRef.current.x(),
                     y: imageRef.current.y()
                    }
        })
      }}
      onTransformEnd={updateShape}
      opacity={shape.opacity}
      blurRadius={shape.blurRadius}
      noise={shape.noise}
      embossStrength={shape.embossStrength}
      embossWhiteLevel={0}
      embossBlend
      draggable
      scale={{x: shape.scaleX, y: shape.scaleY}}
      sepia={shape.sepia}
      grayScale={shape.grayScale}
      filters={[Konva.Filters.Blur,Konva.Filters.Noise, Konva.Filters.Emboss, ...(shape.sepia ? [Konva.Filters.Sepia] : []), ...(shape.grayScale ? [Konva.Filters.Grayscale] : []),]} 
    />




{(imageRef.current && !cropping) &&  (<Transformer 
          node={imageRef.current} 
          rotateEnabled={false}
          draggable
    
          />)}

</>

  );
}


