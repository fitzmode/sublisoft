import React, { useEffect, useRef } from 'react';
import { RegularPolygon } from "react-konva";
import { EditorActions, IAction } from '../context/context';
import Konva from 'konva';

type ShapeProps = {
  dispatch: React.Dispatch<IAction>,
  shape:any;
}

export const PolygonShape: React.FunctionComponent<ShapeProps> = ({ shape, dispatch }) => {

  useEffect(() => {
    dispatch({ type: EditorActions.StateChanged })

    return () => {
    dispatch({ type: EditorActions.StateChanged })
    }
}, [])

  return (
    <RegularPolygon 
    id={shape.uid}
    x={shape.x} 
    y={shape.y} 
    radius={shape.radius}
    sides={shape.sides}
    fill={shape.fill}
    scale={{x: shape.scaleX, y: shape.scaleY}} 
    opacity={shape.opacity} 
    stroke={shape.stroke}
    strokeWidth={shape.strokeWidth}
    zIndex={shape.zIndex && shape.zIndex}
    draggable/>
  );
}
