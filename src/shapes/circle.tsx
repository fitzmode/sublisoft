import React, { useEffect, useRef } from 'react';
import { Text, Circle } from "react-konva";
import { EditorActions, IAction } from '../context/context';
import Konva from 'konva';

type ShapeProps = {
  dispatch: React.Dispatch<IAction>,
  shape:any;
}

export const CircleShape: React.FunctionComponent<ShapeProps> = ({ shape, dispatch }) => {
  const text = useRef(null)
  useEffect(() => {
    dispatch({ type: EditorActions.StateChanged })

    return () => {
    dispatch({ type: EditorActions.StateChanged })
    }
}, [])

  return (
    <Circle 
    ref={text}
    id={shape.uid}
    radius={shape.radius}
    onMouseDown={() => dispatch({type: EditorActions.SetSelected, payload: shape})} 
    x={shape.x} 
    y={shape.y} 
    opacity={shape.opacity} 
    fill={shape.fill}
    stroke={shape.stroke && shape.stroke}
    strokeWidth={shape.strokeWidth && shape.strokeWidth}
    zIndex={shape.zIndex && shape.zIndex}
    draggable/>
  );
}
