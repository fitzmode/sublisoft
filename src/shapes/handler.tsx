import React, { useRef, useEffect, useCallback } from 'react';
import { Transformer } from "react-konva";
import Konva from 'konva'


type ITransformerProps = {
  image?:any;
  id: string;
}
export const TransfromerComponent: React.FunctionComponent<ITransformerProps> = ({ id }) =>  {
    const transformerRef = useCallback((node : Konva.Transformer) => {
      if(node !== null) {
       const stage =  node.getStage();
       const shape = stage.findOne(`#${id}`);
       if(!shape) return;
        node.attachTo(shape)
      }
    }, [id])

      return (
        <Transformer
          ref={transformerRef}
        /> 
      );
  }