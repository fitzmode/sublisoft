import React, { useEffect, useRef } from 'react';
import { Rect } from "react-konva";
import { EditorActions, IAction } from '../context/context';
import Konva from 'konva';

type ShapeProps = {
  dispatch: React.Dispatch<IAction>,
  shape:any;
}

export const SquareShape: React.FunctionComponent<ShapeProps> = ({ shape, dispatch }) => {
  const text = useRef(null)
  // useEffect(() => {
  //   if(text.current) {

  //    text.current.cache();
  //    dispatch({type: EditorActions.UpdateShape, payload: { width: text.current.width(), height: text.current.height()}})
  //   }
  // }, [text.current])
  return (
    <Rect 
    ref={text}
    id={shape.uid}
    x={shape.x} 
    y={shape.y} 
    fill={shape.fill}
    width={shape.width}
    height={shape.height}
    scale={{x: shape.scaleX, y: shape.scaleY}} 
    opacity={shape.opacity} 
    stroke={shape.stroke}
    strokeWidth={shape.strokeWidth}
    zIndex={shape.zIndex && shape.zIndex}
    draggable/>
  );
}
