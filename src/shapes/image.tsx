import React, { useRef, useEffect, useState, Fragment, forwardRef, useCallback, cloneElement} from 'react';
import { Image, Group, Rect } from 'react-konva';
import useImage from 'use-image';
import { EditorActions, IAction } from '../context/context';
import Konva from 'konva';
import { TransfromerComponent } from './handler';


type IShapeProps = {
  shape:any;
  cropping: boolean;
  dispatch: React.Dispatch<IAction>
}

 export const ImageComponent: React.FunctionComponent<IShapeProps> = React.memo(({ shape, dispatch, cropping }) => {


  const [image] = useImage(shape.url ,'Anonymous');
  const imageRef = useRef(null);
  const updateSelected = () => {
    dispatch({type: EditorActions.SetSelected, payload: shape})
  }

  useEffect(() => {
    if(!image) return;
    const layer = imageRef.current.getLayer();
    imageRef.current.cache()
    layer.batchDraw()
  }, [shape])

  const updateShape = ({ currentTarget }) => {
    dispatch({
      type: EditorActions.UpdateShape, 
      payload: { 
                 scaleX: currentTarget.scaleX(), 
                 scaleY: currentTarget.scaleY(),
                 x: currentTarget.x(),
                 y: currentTarget.y()
                }
    })
    dispatch({ type: EditorActions.StateChanged })
  }

  const onChangeCrop = (crop) => {
    dispatch({type: EditorActions.UpdateCropSelection, payload: crop})
  }

  return (
     <ReactKonvaImageCrop onChangeCrop={onChangeCrop} ref={{imageRef}} crop={shape.cropTo} condition={cropping}>
      <Image 
        id={shape.uid}
        ref={imageRef}
        image={image}
        height={shape.height}
        width={shape.width}
        onMouseDown={updateSelected} 
        onTransformEnd={updateShape}
        onDragEnd={updateShape}
        opacity={shape.opacity}
        blurRadius={shape.blurRadius}
        noise={shape.noise}
        embossStrength={shape.embossStrength}
        embossWhiteLevel={0}
        embossBlend
        offset={{x: shape.flipX ? shape.width/2 : 0, y: shape.flipY ? shape.height/2 : 0}}
        scale={{x: shape.flipX ? -1  : shape.scaleX, y: shape.flipY ? -1 * shape.scaleY : shape.scaleY}}
        sepia={shape.sepia}
        x={ shape.x }
        y={ shape.flipY ? shape.y + (shape.height/2) : shape.y }
        draggable
        grayScale={shape.grayScale}
        filters={[Konva.Filters.Blur,Konva.Filters.Noise, Konva.Filters.Emboss, ...(shape.sepia ? [Konva.Filters.Sepia] : []), ...(shape.grayScale ? [Konva.Filters.Grayscale] : []),]} 
      /> 
  </ReactKonvaImageCrop>
  );
})


const ReactKonvaImageCrop: React.FunctionComponent<any>  = forwardRef(({condition, crop ,onChangeCrop, children}, refs:any) => {
 const {imageRef} = refs;
 const cropper = useRef(null);

 console.log(crop, "Shape crop")



 const _onChangeCrop = ({ currentTarget }) => {
    onChangeCrop({
      x: -1 * currentTarget.x(), 
      y:  currentTarget.y(), 
      width: currentTarget.width() * currentTarget.scaleX(),
      height: currentTarget.height() * currentTarget.scaleY(),

    })
 }


 const switchDragBounds = (pos) => {
  let bounds, newX, newY;
  switch (true) {
    // Image is flipped on both axis
    case (image.scaleX() < 0 && image.scaleY() < 0):
      bounds = {
        left: image.x() - image.width(),
        right: image.x() - image.width() - cropper.current.width(),
        top: Math.abs(image.y()),
        bottom: Math.abs(image.y() + (image.height() * image.scaleY()) - cropper.current.height()),
      };

      newX = (pos.x < bounds.left ? bounds.left : pos.x);
      newX = (newX > bounds.right ? bounds.right : newX);
       newY = (pos.y < bounds.top ? bounds.top : pos.y);
      newY = (newY > bounds.bottom ? bounds.bottom : newY);
      
      return {
          x: newX,
          y: newY
        }

    case (image.scaleX() < 0 && image.scaleY() >  0) :
      // Image is flipped on x axis
        bounds = {
          left: image.x() - image.width(),
          right: image.x() - cropper.current.width(),
          top: Math.abs(image.y()),
          bottom: Math.abs(image.y() + (image.height() * image.scaleY()) - cropper.current.height()),
        };

        newX = (pos.x < bounds.left ? bounds.left : pos.x);
        newX = (newX > bounds.right ? bounds.right : newX);
        newY = (pos.y < bounds.top ? bounds.top : pos.y);
        newY = (newY > bounds.bottom ? bounds.bottom : newY);

    return {
        x: newX,
        y: newY
      }
   

  // Image is flipped on y axis
  case (image.scaleX() > 0 && image.scaleY() <  0) :
    bounds = {
      left: image.x(),
      right: image.x() + image.width() - cropper.current.width(),
      top: image.y(),
      bottom: image.y() + image.height() - cropper.current.height(),
    };
     newX = (pos.x < bounds.left ? bounds.left : pos.x);
    newX = (newX > bounds.right ? bounds.right : newX);
     newY = (pos.y < bounds.top ? bounds.top : pos.y);
    newY = (newY > bounds.bottom ? bounds.bottom : newY);
    return {
        x: newX,
        y: newY
      }
  
    default:
      bounds = {
        left: image.x() - image.width(),
        right: image.x(),
        top: Math.abs(image.y()),
        bottom: Math.abs(image.y() + (image.height() * image.scaleY()) - cropper.current.height()),
      };

       newX = (pos.x < bounds.left ? bounds.left : pos.x);
      newX = (newX > bounds.right ? bounds.right : newX);
       newY = (pos.y < bounds.top ? bounds.top : pos.y);
      newY = (newY > bounds.bottom ? bounds.bottom : newY);
      return {
          x: newX,
          y: newY
        }
      
  }
 }


 const switchCropBounds = () => {
  switch (true) {
    // Image is flipped on both axis
    case (image.scaleX() < 0 && image.scaleY() < 0):
      
      return {
        ...crop,
        x: crop.x - crop.width,
        y: crop.y
      }

    case (image.scaleX() < 0 && image.scaleY() >  0) :
      // Image is flipped on x axis
    return {
      ...crop,
      x: crop.x - crop.width,
      y: crop.y
    }


  // Image is flipped on y axis
  case (image.scaleX() > 0 && image.scaleY() <  0) :
    const pros = {
      ...crop,
      x: crop.x,
      y: crop.y 
    }
    console.log(pros, "Crop properties")
    return pros

    default:
      return {
        ...crop,
        x: crop.x - crop.width,
        y: crop.y
      }

      
  }
 }


 const image = imageRef.current;
 return condition ? 
<>

 {/* Original image hidden behind */}
 {children}

 {/* Semi transparent layer */}
 <Rect  
        width={image.width()} 
        scaleX={image.scaleX()}
        scaleY={image.scaleY()}
        height={image.height()} 
        fill={'rgba(255,255,255,0.5)'} 
        x={image.x()}
        y={image.y() + (image.height() / 2)}/>


 {/* Dummy Overlay image */}
 {image && 
  <Group
  x={image && image.x()}
  y={image && image.y() + (image.height()/2)}
  scaleX={Math.abs(image.scaleX())}
  scaleY={Math.abs(image.scaleY())}
  offset={{x: 0, y: image.height()/2}}
  width={image.width()}
  height={image.height()}
  clipFunc={(ctx) => {
    ctx.save();
    ctx.translate(cropper.current.x(), cropper.current.y())
    // ctx.rotate(Konva.getAngle(fakeShape.rotation()))
    ctx.rect(0, 0, cropper.current.width() * cropper.current.scaleX(), cropper.current.height() * cropper.current.scaleY());
    ctx.restore()
  }}
  
  >

 <Image 
    image={image.image()}
    width={image.width()}
    height={image.height()}
    scaleX={image.scaleX()}
    scaleY={image.scaleY()}
   />




      <Rect ref={cropper}
          id={'_cropper'}
           onDragEnd={_onChangeCrop}
           onTransformEnd={_onChangeCrop}
          //  fill={'#ff4081'}
           height={crop.height || 100} 
           width={crop.width || 100}
           x={0}
           y={image.y() - image.height() - image.height()}
           dragBoundFunc={(pos) => {
            return switchDragBounds(pos)
           }}
          fill={'rgba(0,0,0,0)'} 
          draggable />
<TransfromerComponent image={image} id={'_cropper'}/> 

   </Group> }
   </> : React.Children.map(children, child => (
    React.cloneElement(child, {...(crop.x ? { crop: {
    ...switchCropBounds()
    }, width: crop.width, height: crop.height } : {  })})))
   
})

